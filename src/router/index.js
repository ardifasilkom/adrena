import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import swal from 'sweetalert2'

import Authentication from '@/components/AppAuthentication'
import Dashboard from '@/components/AppDashboard'
import Login from '@/components/BaseAuthLogin'

import EventDetail from '@/components/BaseEventDetail'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
  } else {
    next('/')
  }
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
  } else {
    next('/')
  }
}

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Authentication,
      beforeEnter: ifNotAuthenticated,
      children: [
        {
          path: '/login',
          name: 'login',
          component: Login
        }
      ]
    },
    {
      path: '/auth',
      component: Dashboard,
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: '/event',
          name: 'event',
          component: EventDetail
        }
      ]
    }
  ]
})

router.afterEach((to, from) => {
  store.dispatch('loadNewInboxUnread')
    .catch(error => {
      if (error.response.status !== 401) {
        swal('Error get new inbox', error.response.status, 'error')
      }
    })
})

export default router
