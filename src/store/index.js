import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import event from './modules/event'
import inbox from './modules/inbox'
import profile from './modules/profile'
import payment from './modules/payment'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    event,
    inbox,
    profile,
    payment
  }
})
