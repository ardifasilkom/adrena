import axios from 'axios'

const getDefaultData = () => {
  return {
    settingPassword: {
      old_password: '',
      new_password: '',
      retype_password: ''
    },
    personalData: {
      name: '',
      photo: {},
      country_of_birth: '',
      date_of_birth: '',
      gender: '',
      nationality: '',
      street_address: '',
      city: '',
      state_province_region: '',
      zip_postal_code: '',
      country: ''
    }
  }
}

const state = getDefaultData()

const actions = {
  updateSettingPassword ({ state }) {
    return new Promise((resolve, reject) => {
      axios.post('profile/change-password', state.settingPassword)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadProfilePersonalData ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('profile/personal-data')
        .then(response => {
          commit('setProfilePersonalDataName', response.data.data.name)
          commit('setProfilePersonalDataPhoto', response.data.data.photo)
          commit('setProfilePersonalDataCountryOfBirth', response.data.data.country_of_birth)
          commit('setProfilePersonalDataDateOfBirth', response.data.data.date_of_birth)
          commit('setProfilePersonalDataGender', response.data.data.gender)
          commit('setProfilePersonalDataNationality', response.data.data.nationality)
          commit('setProfilePersonalDataStreetAddress', response.data.data.street_address)
          commit('setProfilePersonalDataCity', response.data.data.city)
          commit('setProfilePersonalDataStateProvinceRegion', response.data.data.state_province_region)
          commit('setProfilePersonalDataZipPostalCode', response.data.data.zip_postal_code)
          commit('setProfilePersonalDataCountry', response.data.data.country)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  updateProfilePersonalData ({ state }) {
    return new Promise((resolve, reject) => {
      axios.post('profile/personal-data', state.personalData)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  deleteProfilePersonalDataPhoto ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.post('v1/profile/delete-picture')
        .then(response => {
          commit('resetProfilePersonalDataPhoto')
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

const mutations = {
  setSettingPasswordOldPassword (state, oldPassword) {
    state.settingPassword.old_password = oldPassword
  },
  setSettingPasswordNewPassword (state, newPassword) {
    state.settingPassword.new_password = newPassword
  },
  setSettingPasswordRetypeNewPassword (state, retypePassword) {
    state.settingPassword.retype_password = retypePassword
  },
  setProfilePersonalDataName (state, name) {
    state.personalData.name = name
  },
  setProfilePersonalDataCountryOfBirth (state, country) {
    state.personalData.country_of_birth = country
  },
  setProfilePersonalDataDateOfBirth (state, date) {
    state.personalData.date_of_birth = date
  },
  setProfilePersonalDataAge (state, age) {
    state.personalData.age = age
  },
  setProfilePersonalDataGender (state, gender) {
    state.personalData.gender = gender
  },
  setProfilePersonalDataNationality (state, nationality) {
    state.personalData.nationality = nationality
  },
  setProfilePersonalDataStreetAddress (state, address) {
    state.personalData.street_address = address
  },
  setProfilePersonalDataCity (state, city) {
    state.personalData.city = city
  },
  setProfilePersonalDataStateProvinceRegion (state, value) {
    state.personalData.state_province_region = value
  },
  setProfilePersonalDataZipPostalCode (state, value) {
    state.personalData.zip_postal_code = value
  },
  setProfilePersonalDataCountry (state, country) {
    state.personalData.country = country
  },
  setProfilePersonalDataPhoto (state, photo) {
    state.personalData.photo = photo
  },
  resetProfilePersonalDataPhoto (state) {
    state.personalData.photo = {}
  }
}

export default {
  state,
  actions,
  mutations
}
