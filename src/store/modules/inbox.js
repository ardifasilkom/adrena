import axios from 'axios'

const dummy = {
  listInbox: [
    {
      uuid: '1',
      title: 'Your Accepted',
      thumbnail: 'https://drive.google.com/uc?id=1ttqV07TcBmHG6bSEjnm4_Kb8vbz0czHN',
      read: false,
      body: 'Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type Lorem Ipsum has been the industry standard dummy text ever since the 1500s, ....'
    },
    {
      uuid: '2',
      title: 'Your Accepted',
      thumbnail: 'https://drive.google.com/uc?id=1ttqV07TcBmHG6bSEjnm4_Kb8vbz0czHN',
      read: true,
      body: 'Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type Lorem Ipsum has been the industry standard dummy text ever since the 1500s, ....'
    }
  ],
  listInboxPageLength: 6,
  listInboxCurrentPage: 1,
  newInboxUnread: true,
  inboxDetail: {
    event: 'Youtex (youth excursion)',
    thumbnail: 'https://drive.google.com/uc?id=1ttqV07TcBmHG6bSEjnm4_Kb8vbz0czHN',
    title: 'Your Accepted',
    body: `
      <p>Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,.<p>
      <p>when an unknown printer took a galley of type Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,.</p>
      <p>Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,.Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,.Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,.</p>
    `,
    document: {
      loa: 'https://drive.google.com/uc?id=1ttqV07TcBmHG6bSEjnm4_Kb8vbz0czHN',
      proposal: 'https://drive.google.com/uc?id=1ttqV07TcBmHG6bSEjnm4_Kb8vbz0czHN',
      proposal_parents: 'https://drive.google.com/uc?id=1ttqV07TcBmHG6bSEjnm4_Kb8vbz0czHN'
    }
  }
}

let data = {
  listInbox: []
}

const getDefaultState = () => {
  return {
    data
  }
}

const state = getDefaultState()

const actions = {
  loadListInbox ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('inbox')
        .then(response => {
          commit('setListInbox', response.data.data)
          // commit('setListInboxPageLength', response.data.last_page)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadNewInboxUnread ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('')
        .then(response => {
          commit('setNewInboxUnread', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  updateNewInboxUnread ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.put('', { data: false })
        .then(response => {
          commit('setNewInboxUnread', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadInboxDetail ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('')
        .then(response => {
          commit('setInboxDetail', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  updateInboxReadStatus () {
    return new Promise((resolve, reject) => {
      axios.put('')
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

const mutations = {
  resetInboxState (state) {
    Object.assign(state, getDefaultState())
  },
  setListInbox (state, listInbox) {
    state.listInbox = listInbox
  },
  setListInboxPageLength (state, pageLength) {
    state.listInboxPageLength = pageLength
  },
  setListInboxCurrentPage (state, page) {
    state.listInboxCurrentPage = page
  },
  setNewInboxUnread (state, newInbox) {
    state.newInboxUnread = newInbox
  },
  setInboxDetail (state, inboxDetail) {
    state.inboxDetail = inboxDetail
  }
}

export default {
  state,
  actions,
  mutations,
  dummy
}
