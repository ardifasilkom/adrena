import axios from 'axios'
import { stat } from 'fs';

const dummy = {
  paymentForm:[
    {
      id: 1,
      field_name: "Facility",
      status: "",
      value: "",
      description: "",
      form_group: []
    },
    {
      id: 2,
      field_name: "Ticket",
      status: "",
      value: "",
      form_group: []
    },
    {
      id: 3,
      field_name: "Payment Type",
      status: "",
      value: "",
      form_group: []
    },
    {
      id: 4,
      field_name: "Payer",
      status: "",
      value: "",
      form_group: []
    },
    {
      id: 5,
      field_name: "Payment Method",
      status: "",
      value: "",
      form_group: []
    }
  ],
  paymentAccomodation: [
    {
      id: 1,
      isRecommend: true,
      title: 'Full Accomodation',
      total_benefit : "3",
      price: '400',
      deadline: '20 Agustus 2018',
      benefit: [
        {
          id: '1',
          description: 'benefit 1'
        },
        {
          id: '2',
          description: 'benefit 2'
        },
        {
          id: '3',
          description: 'benefit 3'
        }
      ]
    },
    {
      id: 2,
      isRecommend: false,
      title: 'Non Accomodation',
      total_benefit : "2",
      price: '360',
      deadline: '20 Agustus 2018',
      benefit: [
        {
          id: '1',
          description: 'benefit 1'
        },
        {
          id: '2',
          description: 'benefit 2'
        }
      ]
    }
  ],
  paymentType:[
    {
      id: 1,
      name: "Full Payment",
      duedate: 'Jul 28, 2019',
      form_group:[]
    },
    {
      id: 2,
      name: "Installment",
      duedate: '',
      form_group:[
        {
          id: 1,
          name: "1'st Installment",
          amount: "200",
          duedate: "Jul 31, 2019"
        },
        {
          id: 2,
          name: "2'nd Installment",
          amount: "400",
          duedate: "Sep 31, 2019"
        }
      ]
    }
  ],
  delegateDetail: 
    {
      id: 1,
      name: "Rachmat Ardiansyah",
      email: "ardifasilkom@gmail.com"
    },
  paymentProgress: [
    {
      title: 'Event Payment',
      progress: 20
    },
    {
      title: 'Payment Type',
      progress: 30
    },
    {
      title: 'Payment Method',
      progress: 40
    },
    {
      title: 'Checkout',
      progress: 60
    }
  ],
  eventPayment: {
    banner: 'https://drive.google.com/uc?id=1o4sCJyDYFVwWnrb5JCaCisn5DeNoGpSp',
    payment_type: [
      {
        id: 1,
        title: 'Normal Payment dummy',
        image: 'https://drive.google.com/uc?id=1M9bZjFg7N682B14wldOMBqwFLJ0eY7la',
        days_left: 10
      },
      {
        id: 2,
        title: 'Early Bird Payment',
        image: 'https://drive.google.com/uc?id=1M9bZjFg7N682B14wldOMBqwFLJ0eY7la',
        days_left: 10
      }
    ]
  },
  paymentMethod: {
    banner: 'https://drive.google.com/uc?id=1o4sCJyDYFVwWnrb5JCaCisn5DeNoGpSp',
    event_payment: 'Early-Bird Payment',
    payment_type: 'Full-Accomodation',
    payment_method: [
      {
        id: 1,
        title: 'One Pay',
        step: [
          {
            title: '',
            price: 440,
            days_left: 30
          }
        ]
      },
      {
        id: 2,
        title: 'Two Pay',
        step: [
          {
            title: '1st Payment',
            price: 240,
            days_left: 30
          },
          {
            title: '2nd Payment',
            price: 200,
            days_left: 60
          }
        ]
      }
    ]
  },
  paymentCheckout: {
    payment_session: [
      {
        id: 1,
        title: '1st Payment Full Accommodation (Normal) (240 USD)',
        done: true,
        day_left: 0
      },
      {
        id: 2,
        title: '2nd Payment Full Accommodation (Normal) (200 USD)',
        done: false,
        day_left: 30
      }
    ],
    your_order: [
      {
        id: 1,
        title: '2nd Payment Full Accommodation (Normal)',
        price: 200
      }
    ],
    order_total: 200,
    transfer_method: [
      {
        id: 1,
        title: 'PayPal',
        info: true
      },
      {
        id: 2,
        title: 'Online Payment using Indonesian Bank',
        info: true
      },
      {
        id: 3,
        title: 'Bank Transfer',
        info: true
      }
    ]
  },
  transfer: {
    order_number: '#289890',
    deadline_transfer: 'Nov 5, 2019 15:37:25',
    amount: 200,
    status: 'Waiting for Payment',
    evidance: 'https://drive.google.com/uc?id=1M9bZjFg7N682B14wldOMBqwFLJ0eY7la'
  },
  invoice: {
    order_number: '#289890',
    event_logo: 'https://drive.google.com/uc?id=1ttqV07TcBmHG6bSEjnm4_Kb8vbz0czHN',
    event_title: 'Youtex',
    total: 200,
    date: '23 December 2018',
    transfer_via: 'Bank Transfer',
    description: `
      <p>If you are our selected delegates and you want to pay to complete your registration, then please fill the form based on your registration data in our program.</p>
      <p>If you are not our delegates paying on behalf of our selected delegate(s) that you know daughter/friend’s/team/member, then please fill the form based on our delegate’s registration data not your data.</p>
      <p>If you really need the receipt to be sent to your email address and need to fill the Billing Details based on your data for various purposes, then you can do so.</p>
      <p>However, <strong>please send us email confirmation containing the data of the delegate that you are paying for</strong> and also <strong>attach the receipt in the email</strong></p>
    `,
    status: 'Waiting for your Payment',
    transfer: true,
    download: 'https://drive.google.com/uc?id=1M9bZjFg7N682B14wldOMBqwFLJ0eY7la'
  }
}

let data = {
  choosenPaymentType: {},
  choosenPaymentMethod: {},
  activePaymentPlan: {},
  paymentProgress: [],
  eventPayment: {},
  paymentMethod: {},
  paymentCheckout: {},
  transfer: {},
  invoice: {},
  paymentAccomodation: [],
  paymentTicket: [],
  paymentForm: [],
  delegateDetail: {},
  paymentType: {},
  paymentSubmitStatus: {},
  submittedPaymentSummary: {},
  paymentList: {},
  paymentFacility: {
    paymentFacility: null
  },
  couponName: {
    name_coupon: ''
  },
  paymentPlanSummary : {},
  paymentSummary: [
    {
      category: "Facility",
      name: null,
      amount: null,
      form_group:[]
    },
    {
      category: "Discount",
      name: null,
      amount: null,
      form_group:[]
    },
    {
      category: "Payment Type",
      name: null,
      amount: null,
      form_group:[]
    },
    {
      category: "Payment Method",
      name: null,
      amount: 30,
      form_group:[]
    }
  ],
  bankAccount: {},
  onGoingPayment : {}
}

const getDefaultState = () => {
  return data
}

const state = getDefaultState()

const actions = {
  loadPaymentForm ({ commit }, {uuidEvent}) {
    return new Promise((resolve, reject) => {
      axios.get('/payment/payment-form-label/' + uuidEvent)
        .then(response => {
          commit('setPaymentForm', response.data.paymentForm)
          commit('setPaymentSubmitStatus', response.data.payment_submit_status)
          commit('setSubmittedPaymentSummary', response.data.payment_summary)
          commit('setPaymentList', response.data.payment_list)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPaymentAccomodation({commit}, {uuidEvent}) {
    return new Promise((resolve, reject) => {
      axios.get('/payment/payment-form-accomodation/' + uuidEvent)
        .then(response => {
          commit('setPaymentAccomodation', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPaymentTicket({commit}, {uuidEvent}) {
    return new Promise((resolve, reject) => {
      axios.get('/payment/payment-form-ticket/' + uuidEvent)
        .then(response => {
          commit('setPaymentTicket', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPaymentType({commit}, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.post('/payment/payment-form-plan/' + uuidEvent, state.paymentFacility)
      .then(response => {
        commit('setPaymentType', response.data.data)
        console.log({"disini response" : response})
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
  },
  loadDelegateDetail({commit}) {
    commit('setDelegateDetail', dummy.delegateDetail)
  },
  loadPaymentProgress ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('payment/progress/' + uuidEvent)
        .then(response => {
          commit('setPaymentProgress', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventPayment ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('payment/event-payment/' + uuidEvent)
        .then(response => {
          commit('setEventPayment', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitEventPayment ({ state, commit }, { uuidEvent, paymentType }) {
    return new Promise((resolve, reject) => {
      axios.post('/payment/submit-payment/' + uuidEvent, {
        form: state.paymentForm,
        summary: state.paymentSummary,
      })
        .then(response => {
          commit('setBankAccount', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitPaymentType ({ state }, { uuidEvent, paymentType }) {
    return new Promise((resolve, reject) => {
      axios.post('payment/payment-type/' + uuidEvent, { payment_type: paymentType })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPaymentMethod ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('payment/payment-method/' + uuidEvent)
        .then(response => {
          commit('setPaymentMethod', response.data.data)
          resolve(response)
          console.log("Disini")
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitPaymentMethod ({ state }, { uuidEvent, paymentMethod }) {
    return new Promise((resolve, reject) => {
      axios.post('payment/payment-method/' + uuidEvent, { payment_method: paymentMethod })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPaymentCheckout ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('payment/payment-checkout/' + uuidEvent)
        .then(response => {
          commit('setPaymentCheckout', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitPaymentCheckout ({ state }, { uuidEvent, paymentMethod }) {
    return new Promise((resolve, reject) => {
      axios.post('payment/payment-checkout/' + uuidEvent, { transfer_method: paymentMethod })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPaymentTransfer ({ commit, state }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('payment/payment-transfer/' + uuidEvent)
        .then(response => {
          commit('setPaymentTransfer', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPaymentInvoice ({ commit }, { uuidEvent, invoiceId }) {
    return new Promise((resolve, reject) => {
      axios.get('payment/payment-invoice/' + uuidEvent + '/' + invoiceId)
        .then(response => {
          commit('setPaymentInvoice', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitPaymentMidtrans ({ commit }, { uuidEvent, price }) {
    return new Promise((resolve, reject) => {
      axios.post('payment/payment-midtrans/' + uuidEvent, { price })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitPaymentPaypal ({ commit }, { uuidEvent, status }) {
    return new Promise((resolve, reject) => {
      axios.post('payment/payment-paypal/' + uuidEvent, { status })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitCoupon ({state, commit, dispatch},{ uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.post('/payment/payment-form-coupon/' + uuidEvent, state.couponName)
        .then(response => {
          commit('setCouponSubmit', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitSucceedStatus({commit}, {uuidEvent}){
    return new Promise((resolve, reject) => {
      axios.post('/payment/accept/' + uuidEvent)
      .then(response =>{
        resolve(response)
      })
    })
  },
  submitOnHoldStatus({commit}, {uuidEvent}){
    return new Promise((resolve, reject) => {
      axios.post('/payment/on-hold/' + uuidEvent)
      .then(response =>{
        resolve(response)
      })
    })
  },
  loadPaymentTransaction ({commit}, {uuidEvent}) {
    return new Promise((resolve, reject) => {
      axios.get('/payment/payment-transaction/' + uuidEvent)
        .then(response => {
          commit('setPaymentPlanSummary', response.data.data)
          commit('setActivePaymentPlan', response.data.amount)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadOnGoingPayment({commit}, {uuidEvent}) {
    return new Promise((resolve, reject) => {
      axios.get('/payment/on-going/' + uuidEvent)
        .then(response => {
          commit('setOnGoingPayment', response.data.data['expired_date'])
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  // Function load is below
  loadMidtransLink ({commit}, {uuidEvent, price}) {
    return new Promise((resolve, reject) => {
      axios.post('/payment/payment-midtrans/' + uuidEvent, {price})
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

const mutations = {
  resetPaymentState (state) {
    Object.assign(state, getDefaultState())
  },
  setPaymentProgress (state, paymentProgress) {
    state.paymentProgress = paymentProgress
  },
  setEventPayment (state, eventPayment) {
    state.eventPayment = eventPayment
  },
  // This function will be use later
  // setPaymentType (state, paymentType) {
  //   state.paymentType = paymentType
  // },
  setPaymentMethod (state, paymentMethod) {
    state.paymentMethod = paymentMethod
  },
  setPaymentCheckout (state, paymentCheckout) {
    state.paymentCheckout = paymentCheckout
  },
  setPaymentTransfer (state, paymentTransfer) {
    state.transfer = paymentTransfer
  },
  setPaymentInvoice (state, paymentInvoice) {
    state.invoice = paymentInvoice
  },
  setEventPaymentTypeActive (state, index) {
    state.eventPayment.payment_type[index].active = true
  },
  setPaymentTransferEvidance (state, file) {
    state.transfer.evidance = file.url
  },
  setPaymentAccomodation (state, paymentAccomodation) {
    state.paymentAccomodation = paymentAccomodation
  },
  setPaymentTicket (state, paymentTicket) {
    state.paymentTicket = paymentTicket
  },
  setPaymentType (state, paymentType) {
    state.paymentType = paymentType
  },
  setPaymentForm (state, list) {
    state.paymentForm = list
  },
  setDelegateDetail (state, delegateDetail) {
    state.delegateDetail = delegateDetail
  },
  setPayementFormValue (state, {index, value, text, price}) {
    state.paymentForm[index].value = value
    if(index == 4){
      state.paymentSummary[index - 1].name = text
      state.choosenPaymentMethod = value
    } else if (index ==2){
      state.choosenPaymentType = value
    } else if (index != 3) {
      state.paymentFacility.paymentFacility = value
      state.paymentSummary[index].name = text
      state.paymentSummary[index].amount = price
    }
  },
  setCouponCode (state, {value}) {
    state.couponName.name_coupon = value
  },
  setCouponSubmit (state, coupon) {
    state.paymentSummary[1].name = coupon.name
    state.paymentSummary[1].amount = coupon.amount
    state.paymentForm[5].value = coupon.id
  },
  setPaymentPlanSummary (state, list) {
    state.paymentPlanSummary = list
  },
  setActivePaymentPlan (state, active){
    state.activePaymentPlan = active
  },
  setBankAccount (state, bank){
    state.bankAccount = bank
  },
  setPaymentSubmitStatus (state, status){
    state.paymentSubmitStatus = status
  },
  setSubmittedPaymentSummary (state, summary){
    state.submittedPaymentSummary = summary
  },
  setPaymentList (state, paymentList) {
    state.paymentList = paymentList
  },
  setOnGoingPayment (state, payment) {
    state.onGoingPayment = payment
  }
}

export default {
  state,
  actions,
  mutations,
  dummy
}
