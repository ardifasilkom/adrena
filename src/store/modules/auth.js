import Vue from 'vue'
import axios from 'axios'
import swal from 'sweetalert2'

const getDefaultState = () => {
  return {
    token: localStorage.getItem('user-token') || '',
    login: {
      email: '',
      password: '',
      event_slug: ''
    },
    status: '',
    register: {
      email: '',
      name: '',
      password: '',
      event_slug: ''
    },
    forgotPassword: {
      email: ''
    },
    eventLogin: {}
  }
}

const state = getDefaultState()

const actions = {
  submitAuthRequest ({ state, commit, dispatch }) {
    return new Promise((resolve, reject) => {
      axios.post('/artisanLogin', state.login)
        .then(response => {
          const token = response.data.data.token

          localStorage.setItem('user-token', token)

          axios.defaults.headers.common['Authorization'] = token

          commit('setAuthSuccess', token)

          resolve(response)
        })
        .catch(error => {
          commit('setAuthError', error)

          localStorage.removeItem('user-token')
          localStorage.removeItem('user')

          const errorHandler = Vue.helpers.errorHandler(error)
          state.loadingOverviewStatus = 'error'

          if (error.response.status !== 401) {
            swal.fire(errorHandler.message, errorHandler.error, 'error')
          }

          reject(error)
        })
    })
  },
  submitAuthLogout ({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      commit('setAuthLogout')

      localStorage.removeItem('user-token')
      localStorage.removeItem('user')

      delete axios.defaults.headers.common['Authorization']

      resolve()
    })
  },
  submitAuthRegister ({ state }) {
    return new Promise((resolve, reject) => {
      axios.post('auth/register', state.register)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitAuthForgotPassword ({ state }) {
    return new Promise((resolve, reject) => {
      axios.post('auth/forgot-password', state.forgotPassword)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventLogin ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('auth/event/' + uuidEvent)
        .then(response => {
          commit('setEventLogin', response.data.data)
          console.log({"This is the Event Logo ============ " : response.data.data})
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

const mutations = {
  resetAuthState (state) {
    Object.assign(state, getDefaultState())
  },
  setAuthRequest (state) {
    state.status = 'loading'
  },
  setAuthSuccess (state, token) {
    state.status = 'success'
    state.token = token
  },
  setAuthError (state) {
    state.status = 'error'
  },
  setAuthLogout (state) {
    state.status = ''
    state.token = ''
  },
  setAuthEmail (state, email) {
    state.login.email = email
  },
  setAuthLoginEventSlug (state, eventSlug) {
    state.login.event_slug = eventSlug
  },
  setAuthPassword (state, password) {
    state.login.password = password
  },
  setAuthRegisterEmail (state, email) {
    state.register.email = email
    state.login.email = email
  },
  setAuthRegisterEventSlug (state, eventSlug) {
    state.register.event_slug = eventSlug
  },
  setAuthRegisterName (state, name) {
    state.register.name = name
  },
  setAuthRegisterPassword (state, password) {
    state.register.password = password
    state.login.password = password
  },
  setAuthForgotPasswordEmail (state, email) {
    state.forgotPassword.email = email
  },
  setEventLogin (state, event) {
    state.eventLogin = event
  }
}

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status
}

export default {
  state,
  actions,
  mutations,
  getters
}
