import axios from 'axios'

let data = {
  myEvent: [],
  eventDetail: {},
  eventRegistrationForm: [],
  eventCouncilList: [],
  eventCouncilDetail: [],
  eventProgress: [],
  eventProgressLoadingState: '',
  eventPersonalData: [],
  eventContactDetail: [],
  eventEducationLevel: [],
  eventAboutDelegate: [],
  eventDelegateCouncil:[],
  eventTodoList: {},
  eventDocument: [],
  eventDelegatesList: [],
  eventDelegatesDetail: {},
  eventDelegatesCouncilSelect: '',
  eventDocumentProgress: [],
  eventFaqLists: [],
  eventFaqDetail: {},
  countryCouncilList: [],
  parentJobList: [],
  newsList: {}
}

const getDefaultState = () => {
  return data
}

const state = getDefaultState()

const actions = {
  loadNews({ commit }){
    return new Promise((resolve, reject) => {
      axios.get('/newsList?page=1&limit=10',{
        headers : {Scope : "artisan"}
      })
      .then(response => {
        commit('setNewsList', response.data.data.list)
        resolve(response)
      }).catch(error =>{
        reject(error)
      })
    })
  },
  loadMyEvent ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('events')
        .then(response => {
          commit('setMyEvent', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventFaqList ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('event-faq')
        .then(response => {
          commit('setEventFaqList', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventFaqDetail ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('event-faq/' + uuidEvent)
        .then(response => {
          commit('setEventFaqDetail', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventDocumentProgress ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('event-document-progress/' + uuidEvent)
        .then(response => {
          commit('setEventDocumentProgress', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventDocument ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('event-document/' + uuidEvent)
        .then(response => {
          commit('setEventDocument', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitEventDocument ({ state }, { uuidEvent, isSubmit } = { isSubmit: false }) {
    let submitType = ''
    if (isSubmit) {
      submitType = 'submit'
    }
    return new Promise((resolve, reject) => {
      axios.post('event-document/' + uuidEvent + '/' + submitType, state.eventDocument)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  deleteEventDocument ({ commit }, { uuidEvent, uuidDocument }) {
    return new Promise((resolve, reject) => {
      axios.delete('event-document/' + uuidEvent + '/' + uuidDocument)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventTodoList ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('event-todolist/' + uuidEvent)
        .then(response => {
          commit('setEventTodoList', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitEventTodolist ({ commit }, { todolistId }) {
    return new Promise((resolve, reject) => {
      axios.post('event-todolist/' + todolistId)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  deleteEventTodolist ({ commit }, { todolistId }) {
    return new Promise((resolve, reject) => {
      axios.delete('event-todolist/' + todolistId)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventDelegatesList ({ commit }, { uuidCouncil, uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('council-delegate-list/' + uuidEvent + '/' + uuidCouncil)
        .then(response => {
          commit('setEventDelegatesList', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventDelegatesDetail ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('council-event-detail/' + uuidEvent)
        .then(response => {
          commit('setEventDelegatesDetail', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventDetail ({ commit }, { uuidEvent }) {
    this.eventDetailLoadingState = 'fetch'
    return new Promise((resolve, reject) => {
      axios.get('event/' + uuidEvent)
        .then(response => {
          commit('setEventDetail', response.data.data)
          resolve(response)
        })
        .catch(error => {
          this.eventDetailLoadingState = 'error'
          reject(error)
        })
    })
  },
  loadEventRegistrationForm ({ commit }, { uuidEvent, formType }) {
    return new Promise((resolve, reject) => {
      axios.get('event/' + uuidEvent + '/' + formType)
        .then(response => {
          commit('setEventRegistrationForm', { list: response.data.data, formType })
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitEventRegistrationForm ({ state }, { uuidEvent, formType }) {
    return new Promise((resolve, reject) => {
      axios.post('event/' + uuidEvent + '/' + formType, state.eventRegistrationForm)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventCouncilList ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('event/council/' + uuidEvent)
        .then(response => {
          commit('setEventCouncilList', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventCouncilDetail ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('event-council-country-detail/' + uuidEvent)
        .then(response => {
          commit('setEventCouncilDetail', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadCouncilCountryList ({ commit }, { councilId }) {
    return new Promise((resolve, reject) => {
        axios.get('country/' + councilId)
        .then(response => {
          commit('setCouncilCountryList', { list: response.data.data })
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  submitEventCouncil ({ state }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.post('event/' + uuidEvent + '/delegate-council', state.eventRegistrationForm)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventProgress ({ commit, state }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      state.eventProgressLoadingState = 'fetch'
      axios.get('event/progress/' + uuidEvent)
        .then(response => {
          commit('setEventProgress', response.data.data)
          resolve(response)
        })
        .catch(error => {
          state.eventProgressLoadingState = 'error'
          reject(error)
        })
    })
  },
  submitEventRegistration ({ state }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.post('event-submit/' + uuidEvent)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadGenerateLoa ({ commit }, { uuidEvent }) {
    return new Promise((resolve, reject) => {
      axios.get('generate-loa/' + uuidEvent)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadParentJob ({commit}) {
    return new Promise((resolve, reject) => {
        axios.get('parent-jobs/list')
        .then(response => {
          commit('setParentJobList', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
}

const mutations = {
  resetEventState (state) {
    Object.assign(state, getDefaultState())
  },
  setNewsList(state, news){
    state.newsList = news
  },
  setMyEvent (state, myEvent) {
    state.myEvent = myEvent
  },
  setEventFaqList (state, faqList) {
    state.eventFaqLists = faqList
  },
  setEventFaqDetail (state, faqDetail) {
    state.eventFaqDetail = faqDetail

  },
  setEventDocumentProgress (state, progress) {
    state.eventDocumentProgress = progress
  },
  setEventDocument (state, eventDocument) {
    state.eventDocument = eventDocument
  },
  setEventDocumentFile (state, { filename, pathFilename, url, index }) {
    state.eventDocument[index].file.filename = filename
    state.eventDocument[index].file.path_filename = pathFilename
    state.eventDocument[index].file.url = url
  },
  deleteEventDocumentFile (state, index) {
    state.eventDocument[index].file.filename = null
    state.eventDocument[index].file.path_filename = null
    state.eventDocument[index].file.url = null
  },
  setEventTodoList (state, eventTodoList) {
    state.eventTodoList = eventTodoList
  },
  setEventDelegatesList (state, delegatesList) {
    state.eventDelegatesList = delegatesList
  },
  setEventDelegatesDetail (state, delegatesDetail) {
    state.eventDelegatesDetail = delegatesDetail
  },
  setEventDelegatesCouncilSelect (state, select) {
    state.eventDelegatesCouncilSelect = select
  },
  setEventDetail (state, eventDetail) {
    state.eventDetail = eventDetail
    this.eventDetailLoadingState = 'success'
  },
  setEventRegistrationForm (state, { list, formType }) {
    state.eventRegistrationForm = list
    switch (formType) {
      case 'personal-data':
        state.eventPersonalData = list
        break
      case 'contact-detail':
        state.eventContactDetail = list
        break
      case 'education-level':
        state.eventEducationLevel = list
        break
      case 'about-delegate':
        state.eventAboutDelegate = list
        break
      case 'delegate-council':
        state.eventDelegateCouncil = list
        break
      default:
        state.eventRegistrationForm = list
    }
  },
  setEventRegistrationFormValue (state, { index, value }) {
    state.eventRegistrationForm[index].value = value
  },
  setEventPersonalDataStreetAddress (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[0].value = value
  },
  setEventPersonalDataCity (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[1].value = value
  },
  setEventPersonalDataZipPostalCode (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[2].value = value
  },
  setEventPersonalDataStateProvinceRegion (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[3].value = value
  },
  setEventPersonalDataCountry (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[4].value = value
  },
  setEventCouncilList (state, council) {
    state.eventCouncilList = council
  },
  setCouncilCountryList (state, country){
    state.countryCouncilList = country
  },
  setEventCouncilDetail (state, council) {
    state.eventCouncilDetail = council
  },
  setEventCouncilDetailCouncil (state, { index, council }) {
    state.eventCouncilDetail[index].council = council
  },
  setEventCouncilDetailCountryOne (state, { index, value }) {
    state.eventCouncilDetail[index].country_one = value
  },
  setEventCouncilDetailCountryTwo (state, { index, value }) {
    state.eventCouncilDetail[index].country_two = value
  },
  setEventCouncilDetailCountryThree (state, { index, value }) {
    state.eventCouncilDetail[index].country_three = value
  },
  setEventProgress (state, progress) {
    state.eventProgress = progress
    state.eventProgressLoadingState = 'success'
  },
  setFirstDelegateCouncilCountry (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[0].value = value
  },
  setSecondDelegateCouncilCountry (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[1].value = value
  },
  setThirdDelegateCouncilCountry (state, { index, value }) {
    state.eventRegistrationForm[index].form_group[2].value = value
  },
  setParentJobList (state, job) {
    state.parentJobList = job
  },
  setEventPersonalDataPA (state, { value }) {
    state.eventRegistrationForm[10].value = value
  },
  setEventNationalityValue (state, { value }) {
    state.eventRegistrationForm[11].value = value
  }
}

export default {
  state,
  actions,
  mutations
}
