export default {
  errorHandler (error) {
    if (error.response.status === 422) {
      const errorData = error.response.data.error
      let errorArray = []

      for (let key in errorData) {
        if (errorData.hasOwnProperty(key)) {
          errorArray.push(errorData[key])
        }
      }

      const errorConcat = this.flatten(errorArray)

      let errorString = ''
      errorConcat.forEach(error => {
        errorString += ('- ' + error + '<br>')
      })

      const errorObject = {
        message: error.response.data.message,
        error: errorString
      }

      return errorObject
    } else {
      const errorObject = {
        message: error.response.data.message,
        error: ''
      }

      return errorObject
    }
  },
  flatten (arr) {
    return [].concat(...arr)
  }
}
